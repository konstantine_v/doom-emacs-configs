;;; .doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom refresh' after modifying this file!

;; Load other config files such as keybindings
(load! "+bindings.el")                                   ; Load my custom key bindings bindings

;; Other Projectile packages outside
;; These don't follow the typical file convention
(projectile-add-known-project "~/Writings")              ; My LaTeX Writings
(projectile-add-known-project "~/.notes")                ; My Org and Markdown Notes

;; Settings for basic Doom Emacs things
(setq projectile-project-search-path                     ; All the code related stuff that I work on
      '( "~/Code/crystal"                                ; All Crystal Projects
         "~/Code/_sites"                                 ; All SSG Sites
         "~/Code/C"                                      ; All C Projects
         "~/Code/clisp"                                  ; All Coomon Lisp Projects
         "~/Code/elisp"                                  ; All Emacs Lisp Projects
         "~/Code/shell")                                 ; All shell scripts, probably not posix standard
      projectile-sort-order 'recentf                     ; Sort projects by recently opened and woked on
      ;;doom-theme "doom-one"                            ; Customize theme here
      user-full-name "@MaterialFuture"                   ; My name for whatever reason, idk, emails or something
      doom-font (font-spec :family "monospace" :size 13) ; How I like my fonts, pretty universal for all screen types
      org-directory "~/.notes/org/")                     ; Org Directory, in the .notes dir where I like all my notes

;; Activate Packages defined in packages.el
(use-package! rtags)                                     ; For C stuff and clang
