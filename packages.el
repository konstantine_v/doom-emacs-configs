;; -*- no-byte-compile: t; -*-
;;; .doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:host github :repo "username/repo"))
;; (package! builtin-package :disable t)

;;My MELPA Packages
;; (package! org-journal)
(package! rtags)
(package! mu4e)
(package! request)
